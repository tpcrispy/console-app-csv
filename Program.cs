﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace consoleapp {
  class Program {
    static async Task Main(string[] args) {
      //connection string
      string ConString = @"Server=127.0.0.1,1433;User Id=SA;Database=testdatabase;Password=reallyStrongPwd123";
      StringBuilder sb = new StringBuilder();

      using(SqlConnection connection = new SqlConnection(ConString)) {
        connection.Open();
        // GET A LIST OF THE TABLES
        DataTable dt = connection.GetSchema("Tables");
        // LIST TO HOLD THE DATATABLES ONCE WE FILL THEM FROM THE DB
        List < DataTable > ListOfDataTables = new List < DataTable > ();

        Console.WriteLine($"{dt.Rows.Count} TABLES FOUND!");

        foreach(DataRow dataRow in dt.Rows) {
          // DONT WANT TO TRY AND COPY A VIEW
          if ($"{dataRow[3]}".Equals("VIEW")) {
            continue;
          }

          DataTable dataTable = new DataTable();
          string query = $"select * from {(string)dataRow[2]}";
          SqlCommand cmd = new SqlCommand(query, connection);
          SqlDataAdapter da = new SqlDataAdapter(cmd);

          da.Fill(dataTable);
          dataTable.TableName = (string) dataRow[2];
          ListOfDataTables.Add(dataTable);

          IEnumerable < string > columnNames = dataTable.Columns.Cast < DataColumn > ().Select(column => column.ColumnName);
          sb.AppendLine(string.Join(",", columnNames));

          foreach(DataRow row in dataTable.Rows) {
            IEnumerable < string > fields = row.ItemArray.Select(field => field.ToString());
            sb.AppendLine(string.Join(",", fields));
          }

          File.WriteAllText((string) dataRow[2] + ".csv", sb.ToString());
          // add all files to a list to send to the API? 
        }

        //SENDING TO THE API - 
        HttpClient client = new HttpClient();
        var content = new MultipartFormDataContent();

        // todo - get the files dynamically - using hardcoded file for testing first 
        // https://docs.microsoft.com/en-us/dotnet/api/system.io.directory.getfiles?view=net-5.0
        string filePath = Path.Combine("", "Persons.csv");
        byte[] file = System.IO.File.ReadAllBytes(filePath);
        var byteArrayContent = new ByteArrayContent(file);
	
        content.Add(byteArrayContent, "file", "Persons.csv");
        var response = await client.PostAsync("https://localhost:4200/", content);
        string result = await response.Content.ReadAsStringAsync();
      }
    }
  }
}